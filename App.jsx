import React from 'react'
import Web from './src/components/Web'
import { SafeAreaView, StyleSheet, Text } from 'react-native'

const style = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30
  }
})

export default function App() {
  return (
    <SafeAreaView style={style.container}>
      <Web />
    </SafeAreaView>
  )
}
